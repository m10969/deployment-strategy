workspace "MLE Challenge" {
    model {
        fraudDetector = softwareSystem "Fraud detector" "Predicts if a transaction is a fraud or not" {
            model = container "Fraud detection model" "Trained model" "python"
        }

        prod = deploymentEnvironment "Prod" {
            deploymentNode "Amazon Web Services" {
                tags "Amazon Web Services - Cloud"

                sourceCode = infrastructureNode "Source Code" {
                    description "Model implementation"
                    tags "Amazon Web Services - CodeCommit"
                }

                trainingPipeline = infrastructureNode "Training pipeline" {
                    description "Fits the model and publish the resulting artifact"
                    tags "Amazon Web Services - CodePipeline"
                }

                deploymentPipeline = infrastructureNode "Deployment pipeline" {
                    description "Selects the best model and publishes it as a consumable service"
                    tags "Amazon Web Services - CodePipeline"
                }

                executionEnvironment = infrastructureNode "Execution environment" {
                    description "Runs the model in an appropiate environment"
                    tags "Amazon Web Services - SageMaker Model"
                }

                backend = infrastructureNode "Backend" {
                    description "Performs validations, input and output transformations"
                    tags "Amazon Web Services - Lambda Lambda Function"
                }

                apiGW = infrastructureNode "API Gateway" {
                    description "Exposes the backend services"
                    tags "Amazon Web Services - API Gateway"
                }


                artifactsBucket = deploymentNode "Artifacts bucket" "S3" {
                    tags "Amazon Web Services - Simple Storage Service S3 Bucket"

                    modelObject = deploymentNode "Model object" "S3" {
                        tags "Amazon Web Services - Simple Storage Service S3 Object"

                        modelInstance = containerInstance model
                    }
                }

            }
        }

        sourceCode -> trainingPipeline "Triggers"
        trainingPipeline -> modelObject "Publishes"
        deploymentPipeline -> modelObject "Reads"
        deploymentPipeline -> executionEnvironment "Publishes"
        backend -> executionEnvironment "Invokes"
        apiGW -> backend "Redirects to"

    }

    views {
        deployment fraudDetector "Prod" "AmazonWebServicesDeployment" {
            include *
            autolayout

            animation {
                sourceCode
                trainingPipeline
                artifactsBucket modelObject modelInstance
                deploymentPipeline
                executionEnvironment
                backend
                apiGW


            }
        }

        styles {
            element "Element" {
                shape roundedbox
                background #ffffff
            }
            element "Database" {
                shape cylinder
            }
            element "Infrastructure Node" {
                shape roundedbox
            }
        }

        themes https://static.structurizr.com/themes/amazon-web-services-2020.04.30/theme.json
    }

}