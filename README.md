![](structurizr-1-AmazonWebServicesDeployment.png)


To visualize on your local environment

`docker run -it --rm -p 5080:8080 -v $PWD:/usr/local/structurizr structurizr/lite`
